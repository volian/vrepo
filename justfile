#!/usr/bin/env just --justfile

# Run checks
check: spellcheck clippy
	@cargo +nightly fmt --check
	@echo Checks were successful!

# Remove generated artifacts
clean:
	@cargo clean
	@echo Done!

# Build the project
build:
	@cargo build
	@echo Project successfully built!

# Build with custom release profile
release:
	@RUSTFLAGS="-C target-cpu=native" cargo build --profile=lto

# Lint the codebase
clippy +ARGS="":
	@cargo clippy --all-targets --all-features --workspace -- --deny warnings {{ARGS}}
	@echo Lint successful!

# Format the codebase
@fmt +ARGS="":
	#!/bin/sh

	set -e

	cargo +nightly fmt --all -- {{ARGS}}
	echo Codebase formatted successfully!

# Spellcheck the codebase
spellcheck +ARGS="--skip ./target --skip ./.git --skip ./.cargo --skip ./iso":
	@codespell --builtin clear,rare,informal,code --ignore-words-list mut,crate,msdos {{ARGS}}
	@echo Spellings look good!
