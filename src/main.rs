use std::collections::HashMap;
use std::fs;
use std::ops::Deref;
use std::path::{Path, PathBuf};
use std::process::Command;

use anyhow::{anyhow, bail, Context, Result};
use clap::Parser;
use cli::{Cli, Commands, NalaDeps, Repo};
use rust_apt::new_cache;
use rust_apt::package::{Package, Version};
use rust_apt::records::RecordField;
use serde::{Deserialize, Serialize};
mod cli;
mod util;

#[derive(Debug, Serialize, Deserialize)]
pub struct Repos {
	/// Username for logging into your webserver
	username: String,
	/// Webserver DNS or IP
	server: String,
	/// Path to web root
	webroot: String,
	/// Path to Aptly public dir
	aptly: String,
	repo: Vec<Repository>,
}

impl Repos {
	pub fn from_config() -> Result<Self> {
		Ok(toml::from_str::<Repos>(&util::read_to_string(&format!(
			"{}/.config/volian/repo.conf",
			std::env::var("HOME")?
		))?)?)
	}
}

impl Deref for Repos {
	type Target = Vec<Repository>;

	#[inline]
	fn deref(&self) -> &Vec<Repository> { &self.repo }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Repository {
	dist: String,
	gpg: String,
	filepath: String,
}

impl Repository {
	pub fn init(&self) -> Result<()> {
		runcmd!(
			"aptly",
			"repo",
			"create",
			format!("-distribution={}", self.dist),
			// Component is hardcoded until we need that functionality.
			"-component=main",
			format!("volian-{}", self.dist)
		)
	}

	pub fn add_pkgs(&self) -> Result<()> {
		runcmd!(
			"aptly",
			"repo",
			"add",
			format!("volian-{}", self.dist),
			&self.filepath
		)
	}

	pub fn publish(&self) -> Result<()> {
		runcmd!(
			"aptly",
			format!("-gpg-key={}", self.gpg),
			"-origin=Volian",
			"-label=Volian",
			"publish",
			"repo",
			format!("volian-{}", self.dist)
		)
	}

	pub fn publish_drop(&self) -> Result<()> { runcmd!("aptly", "publish", "drop", &self.dist) }

	pub fn repo_drop(&self) -> Result<()> {
		runcmd!("aptly", "repo", "drop", format!("volian-{}", self.dist))
	}

	pub fn update(&self) -> Result<()> { runcmd!("aptly", "publish", "update", &self.dist) }

	/// Used to copy this repository into another
	/// Some examples of pattern:
	///
	/// "Name (%python3*)" - Copies only pkgs with name python3*
	/// "Name (%*)" - Copies all pkgs
	/// "nala" - Copies just pkgs named `nala`
	pub fn copy(&self, to: &Repository, pattern: &str) -> Result<()> {
		runcmd!(
			"aptly",
			"repo",
			"copy",
			format!("volian-{}", self.dist),
			format!("volian-{}", to.dist),
			pattern
		)
	}
}

fn main() -> Result<()> {
	let cli = Cli::parse();

	match &cli.command {
		Commands::Init => {
			let repos = Repos::from_config()?;
			for repo in repos.iter() {
				repo.init()?;
				repo.add_pkgs()?;
			}
			// Copy nala into scar
			repos[0].copy(&repos[1], "nala")?;

			for repo in repos.iter() {
				repo.publish()?;
			}
		},
		Commands::Update => update()?,
		Commands::Drop => drop_all()?,
		Commands::Push(args) => rsync(args)?,
		Commands::NalaDeps(args) => get_nala_deps(args)?,
	}
	Ok(())
}

fn update() -> Result<()> {
	let repos = Repos::from_config()?;
	for repo in repos.iter() {
		repo.add_pkgs()?;
	}
	// Copy nala into scar
	repos[0].copy(&repos[1], "nala")?;

	for repo in repos.iter() {
		repo.update()?;
	}
	Ok(())
}

fn drop_all() -> Result<()> {
	let repos = Repos::from_config()?;
	for repo in repos.iter() {
		// TODO: Find way to only drop if it's published, etc
		repo.publish_drop()?;
		repo.repo_drop()?;
	}
	Ok(())
}

fn rsync(args: &Repo) -> Result<()> {
	let repos = Repos::from_config()?;
	let mut cmd = Command::new("rsync");
	cmd.args([
		"--archive",
		"--verbose",
		"--compress",
		"--checksum",
		"--delete",
		&repos.aptly,
	]);

	let mut target = format!("{}@{}:{}", repos.username, repos.server, repos.webroot);
	target += if args.dev { "devel" } else { "volian" };

	cmd.arg(target);

	let status = cmd
		.status()
		.with_context(|| format!("running {:#?}", cmd))?;

	if !status.success() {
		bail!("{cmd:#?} failed with {status}");
	}
	Ok(())
}

/// Return the package name. Checks if epoch is needed.
fn get_pkg_name(version: &Version) -> String {
	let filename = version
		.get_record(RecordField::Filename)
		.expect("Record does not contain a filename!")
		.split_terminator('/')
		.last()
		.expect("Filename is malformed!")
		.to_string();

	if let Some(index) = version.version().find(':') {
		let epoch = format!("_{}%3a", &version.version()[..index]);
		return filename.replacen('_', &epoch, 1);
	}
	filename
}

fn get_deps(pkg: &Package, nala_deps: &mut Vec<String>) {
	let Some(cand) = pkg.versions().next() else {
		return;
	};

	let Some(deps) = cand.dependencies() else {
		return;
	};

	for dep in deps {
		let pkg = dep.first().target_package();
		let pkg_name = pkg.name().to_string();
		if nala_deps.contains(&pkg_name) {
			continue;
		}
		nala_deps.push(pkg_name);
		get_deps(pkg, nala_deps);
	}
}

fn get_nala_deps(args: &NalaDeps) -> Result<()> {
	let cache = new_cache!()?;
	// In this house nala always exists
	let nala = cache.get("nala").unwrap();

	let mut nala_deps: Vec<String> = vec![];

	get_deps(&nala, &mut nala_deps);

	nala_deps.retain(|p| p.starts_with("python3-"));

	// Get all the packages in the nala directory
	let mut repos = Repos::from_config()?;
	repos.repo.retain(|r| r.dist == "nala");

	let scar = Path::new(&repos.first().context("Nala repo does not exist!")?.filepath);

	let mut scar_pkgs = vec![];
	for file in scar.read_dir()? {
		let os_filename = file?.file_name();

		if let Some(filename) = os_filename.to_str() {
			if filename.ends_with(".deb") {
				let mut final_deb = scar.to_path_buf();
				final_deb.push(filename);

				scar_pkgs.push(final_deb.to_str().unwrap().to_string());
			}
		}
	}

	// Check which packages we can remove
	let cache = new_cache!(&scar_pkgs)?;
	let mut remove_pkgs: Vec<PathBuf> = vec![];
	let mut download_pkgs: HashMap<String, PathBuf> = HashMap::new();

	for pkg_name in &nala_deps {
		let pkg = cache.get(pkg_name).unwrap();

		let versions: Vec<Version> = pkg.versions().collect();
		let cand = pkg.candidate().unwrap();

		for version in &versions {
			for uri in version.uris() {
				if version == &cand {
					let mut filename = scar.to_path_buf();
					filename.push(get_pkg_name(&cand));

					// If the filename exists no reason to download
					if filename.exists() {
						continue;
					}

					// It doesn't exist so we need to download
					download_pkgs.insert(pkg.name().to_string(), filename);
					continue;
				}

				// Split won't have a nth(1) if it's not "file:"
				let Some(split) = uri.split_terminator("file:").nth(1) else {
					continue;
				};

				// If scar contains the file and it's not the candidate remove it.
				let filename = split.to_string();
				if scar_pkgs.contains(&filename) {
					remove_pkgs.push(filename.into())
				}
			}
		}
	}

	// Debug print the package lists
	dbg!(&download_pkgs);
	dbg!(&remove_pkgs);

	if args.only_print {
		println!("Only print proposed changes");
		return Ok(());
	}

	if download_pkgs.is_empty() {
		println!("Nothing new to add");
		return Ok(());
	}

	// Build the command for apt
	// TODO: We should probably add the ability to download in rust-apt
	// Or create libnala?
	let mut cmd = Command::new("apt");
	cmd.arg("download");

	for pkg_name in download_pkgs.keys() {
		cmd.arg(pkg_name);
	}

	let status = cmd
		.status()
		.with_context(|| format!("running {:#?}", cmd))?;
	if !status.success() {
		bail!("{:#?} failed with {}", cmd, status)
	}

	// Move the downloaded files.
	for filename in download_pkgs.values() {
		let source = format!("./{}", filename.file_name().unwrap().to_string_lossy());
		fs::rename(source, filename)?;
	}

	// Remove the files no longer needed
	for filename in remove_pkgs {
		println!("Removing: '{filename:?}'");
		fs::remove_file(filename)?;
	}
	Ok(())
}
