use anyhow::{Context, Result};

/// Runs the provided command. The first macro argument is the executable, and
/// following arguments are passed to the command. Returns a Result<()>
/// describing whether the command failed. Errors are adequately prefixed with
/// the full command.
#[macro_export]
macro_rules! runcmd {
	($cmd:expr) => (runcmd!($cmd,));
	($cmd:expr, $($args:expr),*) => {{
		let mut cmd = Command::new($cmd);
		$( cmd.arg($args); )*
		let status = cmd.status().with_context(|| format!("running {:#?}", cmd))?;
		if !status.success() {
			Result::Err(anyhow!("{:#?} failed with {}", cmd, status))
		} else {
			Result::Ok(())
		}
	}}
}

pub fn read_to_string(path: &str) -> Result<String> {
	std::fs::read_to_string(path).with_context(|| format!("Failed to read '{path}'"))
}
