use clap::{Args, Parser, Subcommand};

#[derive(Parser)]
#[clap(name = "vrepo")]
#[clap(author = "Blake Lee <blake@volian.org>")]
#[clap(version = "0.1.0")]
#[clap(about = "Manage the volian repo", long_about = None)]
pub struct Cli {
	#[command(subcommand)]
	pub command: Commands,
}

#[derive(Subcommand)]
pub enum Commands {
	/// Initialize the repositories
	Init,
	/// Updates all repositories
	Update,
	/// Drops all aptly things to start fresh
	Drop,
	/// Pushes repo to the remote webserver
	Push(Repo),
	/// Update the Nala repo dependencies
	NalaDeps(NalaDeps),
}

/// Runs the Volian repo script
#[derive(Args, Debug)]
pub struct Repo {
	/// Pushes to the development repo
	#[clap(long, action)]
	pub dev: bool,
}

/// Update the Nala repo dependencies
#[derive(Args, Debug)]
pub struct NalaDeps {
	/// Only print proposed changes.
	#[clap(long, action)]
	pub only_print: bool,
}
